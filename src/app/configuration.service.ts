import {Injectable} from '@angular/core';

export const APP_CONFIG = {
  apiEndPoint: '/api'
};

@Injectable({
  providedIn: 'root'
})

export class ConfigurationService {
  getConfiguration(slug: string | number) {
    // @ts-ignore
    return APP_CONFIG[slug];
  }
}
