import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {LoginComponent} from "./login/login.component";
import {AuthGuard} from "./auth.guard";
import {DashboardComponent} from "./core/dashboard/dashboard.component";
import {PatientListComponent} from "./core/patient/patient-list/patient-list.component";
import {PatientDetailComponent} from "./core/patient/patient-detail/patient-detail.component";

const routes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: 'dashboard', component: DashboardComponent, canActivate: [AuthGuard] },
  { path: 'patients', component: PatientListComponent, canActivate: [AuthGuard] },
  { path: 'patient/:uuid', component: PatientDetailComponent, canActivate: [AuthGuard] },

  // Redirect default to dashboard:
  { path: '**', redirectTo: '/login' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
