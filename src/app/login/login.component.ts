import {Component, OnInit} from '@angular/core';
import {UserService} from "../services/user/user.service";
import {UntypedFormBuilder, Validators} from "@angular/forms";
import {Router} from "@angular/router";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  // Password visibility in login form:
  hide = true;
  isLoggedIn: boolean = this.userService.getLoginStatus();

  constructor(private userService: UserService, private formBuilder: UntypedFormBuilder, private router: Router) {
  }

  loginForm = this.formBuilder.group({
    username: ['', Validators.required],
    password: ['', Validators.required]
  });

  ngOnInit(): void {
    if(this.userService.getLoginStatus()) {
      // If the user is loggedin - force redirect to dashboard view
      this.router.navigate(['./dashboard'], {});
    }
  }

  login(): void {
    if (this.loginForm.valid) {
      // Perform login:
      this.userService.login(this.loginForm.get('username')?.value, this.loginForm.get('password')?.value);

      this.userService.loginStatusChange.subscribe(value => {
        if(value) {
          this.router.navigate(['./dashboard'], {});
        }
        else {
          if(this.userService.getUserToken() == "denied") {
            alert("Incorrect username or password. Please try again!");
          }
        }
      });
    }
  }

}
