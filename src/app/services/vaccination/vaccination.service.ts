import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {ConfigurationService} from "../../configuration.service";
import {UserService} from "../user/user.service";
import {Observable} from "rxjs";
import {Vaccination} from "../../_interface/vaccination.model";

@Injectable({
  providedIn: 'root'
})
export class VaccinationService {

  constructor(private http: HttpClient,
              private config: ConfigurationService,
              private userService: UserService) {
  }

  addVaccination(vaccination: Vaccination): Observable<any> {
    let URL = this.config.getConfiguration('apiEndPoint') + `/vaccination/record`;

    return this.http.post(
      URL,
      vaccination,
      {
        headers: this.userService.getHttpHeaders()
      }
    ).pipe();
  }

  updateVaccination(vaccination: Vaccination): Observable<any> {
    let URL = this.config.getConfiguration('apiEndPoint') + `/vaccination/record`;

    return this.http.put(
      URL,
      vaccination,
      {
        headers: this.userService.getHttpHeaders()
      }
    ).pipe();
  }
}
