import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {ConfigurationService} from "../../configuration.service";
import {UserService} from "../user/user.service";
import {MedicalHistory} from "../../_interface/medical_history.model";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class MedicalService {

  constructor(private http: HttpClient,
              private config: ConfigurationService,
              private userService: UserService) {
  }

  addMedicalHistory(medical: MedicalHistory): Observable<any> {
    let URL = this.config.getConfiguration('apiEndPoint') + `/medical/record`;

    return this.http.post(
      URL,
      medical,
      {
        headers: this.userService.getHttpHeaders()
      }
    ).pipe();
  }

  updateMedicalHistory(medical: MedicalHistory): Observable<any> {
    let URL = this.config.getConfiguration('apiEndPoint') + `/medical/record`;

    return this.http.put(
      URL,
      medical,
      {
        headers: this.userService.getHttpHeaders()
      }
    ).pipe();
  }
}
