import {Injectable} from '@angular/core';
import {ConfigurationService} from "../../configuration.service";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Router} from "@angular/router";
import {CookieService} from "../cookie/cookie.service";
import {Subject} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class UserService {

  loginStatus: boolean = false;
  private Configuration = new ConfigurationService();
  private userServiceEndpoint: string = this.Configuration.getConfiguration("apiEndPoint") + "/user";
  protected token: string = "";
  protected userName: string = "";
  protected tenant: string = "";

  loginStatusChange: Subject<boolean> = new Subject<boolean>();
  userNameChange: Subject<string> = new Subject<string>();
  userTokenChange: Subject<string> = new Subject<string>();
  userTenantChange: Subject<string> = new Subject<string>();

  headers = this.getHttpHeaders()

  constructor(private http: HttpClient, private router: Router, private cookie: CookieService) {

    // Init in the case of a page reload:
    this.userName = this.getUserName();
    this.token = this.getUserToken();
    this.tenant = this.getUserTenant();

    this.loginStatusChange.subscribe(value => {
      this.loginStatus = value;
    })

    this.userNameChange.subscribe(value => {
      this.userName = value;
    });

    this.userTokenChange.subscribe(value => {
      this.token = value;
      // Update Headers:
      this.headers = this.getHttpHeaders();
    });

    this.userTenantChange.subscribe(value => {
      this.tenant = value;
      // Update Headers:
      this.headers = this.getHttpHeaders();
    });
  }

  getUserName() {
    if (this.cookie.getCookie("username") != "") {
      this.userName = this.cookie.getCookie("username");
    }

    return this.userName;
  }

  getLoginStatus(): boolean {

    if (this.loginStatus === true || (this.cookie.getCookie("userToken") != "" && this.cookie.getCookie("loginStatus") == "1")) {
      this.loginStatus = true;
    }

    return this.loginStatus;
  }

  getUserToken(): string {
    if (this.cookie.getCookie("userToken") != "") {
      this.token = this.cookie.getCookie("userToken");
      this._updateUserToken();
    }
    return this.token;
  }

  getUserTenant(): string {
    if (this.cookie.getCookie("userTenant") != "") {
      this.tenant = this.cookie.getCookie("userTenant");
      this._updateUserTenant();
    }
    return this.tenant;
  }

  logout() {

    // Fire Login request to backend server:
    this.http.post(
      this.userServiceEndpoint + "/logout",
      null,
      {
        headers: this.headers
      }
    ).subscribe(_data => {
      this.cookie.deleteCookie("userToken");
      this.cookie.deleteCookie("userTenant");
      this.cookie.deleteCookie("username");
      this.cookie.deleteCookie("loginStatus");
      this.loginStatus = false;
      this._updateLoginStatus();
      this._updateUserNameStatus();
      this._updateUserToken();
      this._updateUserTenant();

      // Forward to login view
      this.router.navigate(['./login'], {});
    });
  }

  login(username: string, password: string): void {
    this.http.post(
      this.userServiceEndpoint + "/login",
      {
        username: username,
        password: password
      },
      {
        headers: this.headers
      }
    ).subscribe((data: any) => {
        let status = data.status;

        if (status === "success") {
          this.token = data.token;
          this.userName = data.username;
          this.tenant = data.tenant;
          this.loginStatus = true;

          // Store data to cookie:
          this.cookie.setCookie({
            name: 'loginStatus',
            value: '1',
            expireDays: 1
          });

          this.cookie.setCookie({
            name: 'username',
            value: data.username,
            expireDays: 1
          });

          this.cookie.setCookie({
            name: 'userToken',
            value: data.token,
            expireDays: 1
          });

          this.cookie.setCookie({
            name: 'userTenant',
            value: data.tenant,
            expireDays: 1
          });

          // Update subscriptions:
          this._updateLoginStatus();
          this._updateUserNameStatus();
          this._updateUserToken();
          this._updateUserTenant();

        } else {
          // update to rejected:
          this.loginStatus = false;
          this.token = "denied";
          this.tenant = "";
          this._updateLoginStatus();
          this._updateUserToken();
          this._updateUserTenant();
        }
      },
      (error: any) => {
        // update to rejected:
        this.loginStatus = false;
        this.token = "denied";
        this.tenant = "";
        this._updateLoginStatus();
        this._updateUserToken();
        this._updateUserTenant();
      });
  }

  protected _updateLoginStatus() {
    this.loginStatusChange.next(this.loginStatus);
  }

  protected _updateUserNameStatus() {
    this.userNameChange.next(this.userName);
  }

  protected _updateUserToken() {
    this.userTokenChange.next(this.token);
  }

  protected _updateUserTenant() {
    this.userTenantChange.next(this.tenant);
  }

  public getHttpHeaders(type: string = "HttpHeaders"): any {
    if(type == "object") {
      return {
        "Content-Type": "application/json;charset=utf-8",
        "X-Auth-Tenant": this.tenant,
        "Authorization": "Bearer " + this.token
      };
    }
    else {
      return new HttpHeaders()
        .set("Content-Type", 'application/json;charset=utf-8')
        .set("X-Auth-Tenant", this.tenant)
        .set("Authorization", "Bearer " + this.token);
    }
  }
}
