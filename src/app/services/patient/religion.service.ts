import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {ConfigurationService} from "../../configuration.service";
import {UserService} from "../user/user.service";
import {Patient} from "../../_interface/patient.model";
import {Observable} from "rxjs";
import {Religion} from "../../_interface/religion.model";
import {fromFetch} from "rxjs/internal/observable/dom/fetch";

@Injectable({
  providedIn: 'root'
})
export class ReligionService {

  constructor(private http: HttpClient,
              private config: ConfigurationService,
              private userService: UserService) {
  }


  getReligions(): Religion[] {
    // TODO: Make dynamic request to server:
    return [
      {
        "id": 1,
        "name": "katholisch"
      },
      {
        "id": 2,
        "name": "hindi"
      },
    ]
  }
}
