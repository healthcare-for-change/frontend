import {Injectable} from '@angular/core';
import {Patient} from "../../_interface/patient.model";
import {Observable, Subject} from "rxjs";
import {HttpClient} from "@angular/common/http";
import {ConfigurationService} from "../../configuration.service";
import {UserService} from "../user/user.service";

@Injectable({
  providedIn: 'root'
})
export class PatientService {

  constructor(private http: HttpClient,
              private config: ConfigurationService,
              private userService: UserService) {
  }

  addPatient(patient: Patient): Observable<any> {
    let URL = this.config.getConfiguration('apiEndPoint') + `/patient/store`;

    return this.http.post(
      URL,
      patient,
      {
        headers: this.userService.getHttpHeaders()
      }
    ).pipe();
  }

  updatePatient(patient: Patient): Observable<any> {
    let URL = this.config.getConfiguration('apiEndPoint') + `/patient/store`;

    return this.http.put(
      URL,
      patient,
      {
        headers: this.userService.getHttpHeaders()
      }
    ).pipe();
  }
}
