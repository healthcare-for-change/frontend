import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {ConfigurationService} from "../../configuration.service";
import {UserService} from "../user/user.service";
import {Observable} from "rxjs";
import {Prescription} from "../../_interface/Prescription.model";

@Injectable({
  providedIn: 'root'
})
export class PrescriptionService {

  constructor(private http: HttpClient,
              private config: ConfigurationService,
              private userService: UserService) {
  }

  addPrescription(prescription: Prescription): Observable<any> {
    let URL = this.config.getConfiguration('apiEndPoint') + `/prescription/record`;

    return this.http.post(
      URL,
      prescription,
      {
        headers: this.userService.getHttpHeaders()
      }
    ).pipe();
  }

  updatePrescription(prescription: Prescription): Observable<any> {
    let URL = this.config.getConfiguration('apiEndPoint') + `/prescription/record`;

    return this.http.put(
      URL,
      prescription,
      {
        headers: this.userService.getHttpHeaders()
      }
    ).pipe();
  }
}
