import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {ConfigurationService} from "../../configuration.service";
import {UserService} from "../user/user.service";
import {Observable} from "rxjs";
import {Deworming} from "../../_interface/deworming.model";

@Injectable({
  providedIn: 'root'
})
export class DewormingService {

  constructor(private http: HttpClient,
              private config: ConfigurationService,
              private userService: UserService) {
  }

  addDeworming(deworming: Deworming): Observable<any> {
    let URL = this.config.getConfiguration('apiEndPoint') + `/deworming/record`;

    return this.http.post(
      URL,
      deworming,
      {
        headers: this.userService.getHttpHeaders()
      }
    ).pipe();
  }

  updateDeworming(deworming: Deworming): Observable<any> {
    let URL = this.config.getConfiguration('apiEndPoint') + `/deworming/record`;

    return this.http.put(
      URL,
      deworming,
      {
        headers: this.userService.getHttpHeaders()
      }
    ).pipe();
  }
}
