import { TestBed } from '@angular/core/testing';

import { PhysicalService } from './physical.service';

describe('PhysicalService', () => {
  let service: PhysicalService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PhysicalService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
