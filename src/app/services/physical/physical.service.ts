import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {ConfigurationService} from "../../configuration.service";
import {UserService} from "../user/user.service";
import {Observable} from "rxjs";
import {PhysicalExamination} from "../../_interface/pysical_examination.model";

@Injectable({
  providedIn: 'root'
})
export class PhysicalService {

  constructor(private http: HttpClient,
              private config: ConfigurationService,
              private userService: UserService) {
  }

  addPhysicalExamination(physical: PhysicalExamination): Observable<any> {
    let URL = this.config.getConfiguration('apiEndPoint') + `/physical/record`;

    return this.http.post(
      URL,
      physical,
      {
        headers: this.userService.getHttpHeaders()
      }
    ).pipe();
  }

  updatePhysicalExamination(physical: PhysicalExamination): Observable<any> {
    let URL = this.config.getConfiguration('apiEndPoint') + `/physical/record`;

    return this.http.put(
      URL,
      physical,
      {
        headers: this.userService.getHttpHeaders()
      }
    ).pipe();
  }
}
