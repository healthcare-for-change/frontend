export interface Vaccination {
  uuid: string,
  patient_uuid: string,
  timestamp: Date,
  user_id: number,
  vaccine_uuid: string,
  comment: string|null,
  lot: string|null
}
