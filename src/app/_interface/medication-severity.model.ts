export interface MedicationSeverity {
  id: number,
  name: string
}
