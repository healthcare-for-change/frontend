export interface Patient {
  uuid: string,
  first_name: string,
  name: string,
  gender: string,
  date_of_birth: Date|null,
  date_of_death: Date|null,
  unclear_birthdate: boolean,
  mobile_number: string,
  tenant: string,
  religion_id: number
}
