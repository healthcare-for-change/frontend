export interface MedicalHistory {
  uuid: string,
  patient_uuid: string,
  user_id: number,
  clinical_status: number|null,
  verification_status: number|null,
  severity: number|null,
  note: string|null,
  onset_date: Date|null,
  onset_age: number|null,
  abatement_date: Date|null,
  abatement_age: number|null,
  medical_diagnosis_code_id: number|null,
}
