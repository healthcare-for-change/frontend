export interface MedicationClinicalStatus {
  id: number,
  name: string
}
