export interface Deworming {
  uuid: string,
  patient_uuid: string,
  timestamp: Date|null,
  user_id: number,
  comment: string|null,
  medication_id: string|null
}
