export interface MedicationVerificationStatus {
  id: number,
  name: string
}
