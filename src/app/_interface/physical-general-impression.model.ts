export interface PhysicalGeneralImpression {
  id: number,
  name: string
}
