export interface PhysicalExamination {
  uuid: string,
  patient_uuid: string,
  user_id: number,
  timestamp: Date|null,
  blood_pressure_systolic: number|null;
  blood_pressure_diastolic: number|null;
  heart_rate: number|null;
  weight: number|null;
  height: number|null;
  muac: number|null;
  temperature: number|null;
  skin_examination: string|null;
  details_ent: string|null;
  details_eyes: string|null;
  details_head: string|null;
  details_muscles_bones: string|null;
  details_heart: string|null;
  details_lung: string|null;
  details_gastrointestinal: string|null;
  details_urinary_tract: string|null;
  details_reproductive_system: string|null;
  details_other: string|null;
  general_impression: number|null;
}
