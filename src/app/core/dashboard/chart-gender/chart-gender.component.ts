import {AfterViewInit, Component, OnInit} from '@angular/core';
import {connect, EChartsOption, getInstanceByDom} from "echarts";
import {ConfigurationService} from "../../../configuration.service";
import {UserService} from "../../../services/user/user.service";
import {ThemeOption} from "ngx-echarts";

@Component({
  selector: 'app-chart-gender',
  templateUrl: './chart-gender.component.html',
  styleUrls: ['./chart-gender.component.scss']
})
export class ChartGenderComponent implements OnInit, AfterViewInit {

  data:any = [];
  options: EChartsOption = {};
  theme:ThemeOption = {};

  constructor(
    private config: ConfigurationService,
    private userService: UserService,
  ) {
  }

  ngOnInit(): void {
    this.data = [
      { value: 7, name: 'Male' },
      { value: 15, name: 'Female' },
    ];
  }

  ngAfterViewInit(): void {
    setTimeout(() => {
      let chartElement = document.getElementById("gender-chart");
      // @ts-ignore
      let chart = getInstanceByDom(chartElement);
      // @ts-ignore
      connect([chart]);

      this.updateChart();
    });
  }

  updateChart() {
    let URL = this.config.getConfiguration('apiEndPoint') + `/patient/chartdata_gender`;

    let httpHeaders = this.userService.getHttpHeaders("object");
    fetch(URL, { headers: httpHeaders })
      .then(response => response.json())
      .then( (data:any) => {

        this.options = {
          color: [
            "#cddc39",
            "#dce775",
            "#f0f4c3",
            "#afb42b",
            "#9e9d24",
            "#827717"
          ],
          darkMode: true,
          series: [
            {
              name: 'Access From',
              type: 'pie',
              radius: '50%',
              data: data,
              emphasis: {
                itemStyle: {
                  shadowBlur: 10,
                  shadowOffsetX: 0,
                  shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
              }
            }
          ]
        };

      }, error => {
        console.log(error);
      })
  }

}
