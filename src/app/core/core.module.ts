import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MainMenuComponent} from './main-menu/main-menu.component';
import {MatButtonModule} from '@angular/material/button';
import {MatToolbarModule} from "@angular/material/toolbar";
import {MatMenuModule} from "@angular/material/menu";
import {MatIconModule} from "@angular/material/icon";
import {LoginComponent} from '../login/login.component';
import {MatCardModule} from "@angular/material/card";
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatSelectModule} from "@angular/material/select";
import {MatInputModule} from "@angular/material/input";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {DashboardComponent} from './dashboard/dashboard.component';
import {MatGridListModule} from '@angular/material/grid-list';
import {LayoutModule} from '@angular/cdk/layout';
import { PatientListComponent } from './patient/patient-list/patient-list.component';
import {MatTableModule} from "@angular/material/table";
import {MatProgressBarModule} from "@angular/material/progress-bar";
import {MatPaginatorModule} from "@angular/material/paginator";
import {RouterModule} from "@angular/router";
import { PatientDetailComponent } from './patient/patient-detail/patient-detail.component';
import {MatDividerModule} from "@angular/material/divider";
import {MatTabsModule} from "@angular/material/tabs";
import {MatProgressSpinnerModule} from "@angular/material/progress-spinner";
import { DewormingHistoryComponent } from './deworming/deworming-history/deworming-history.component';
import { PatientFormComponent } from './patient/patient-form/patient-form.component';
import {MatDialogModule} from "@angular/material/dialog";
import {MatRadioModule} from "@angular/material/radio";
import {MatDatepickerModule} from "@angular/material/datepicker";
import {MatCheckboxModule} from "@angular/material/checkbox";
import { SelectRemoteComponent } from './utilities/select-remote/select-remote.component';
import {MatAutocompleteModule} from "@angular/material/autocomplete";
import { DewormingFormComponent } from './deworming/deworming-form/deworming-form.component';
import { MedicalHistoryComponent } from './medical/medical-history/medical-history.component';
import { MedicalFormComponent } from './medical/medical-form/medical-form.component';
import { PhysicalHistoryComponent } from './physical/physical-history/physical-history.component';
import { PhysicalFormComponent } from './physical/physical-form/physical-form.component';
import { VaccinationHistoryComponent } from './vaccination/vaccination-history/vaccination-history.component';
import { VaccinationFormComponent } from './vaccination/vaccination-form/vaccination-form.component';
import { PrescriptionHistoryComponent } from './prescription/prescription-history/prescription-history.component';
import { PrescriptionFormComponent } from './prescription/prescription-form/prescription-form.component';
import {MatSlideToggleModule} from "@angular/material/slide-toggle";
import { ChartGenderComponent } from './dashboard/chart-gender/chart-gender.component';
import {NgxEchartsModule} from "ngx-echarts";

@NgModule({
  declarations: [
    MainMenuComponent,
    LoginComponent,
    DashboardComponent,
    PatientListComponent,
    PatientDetailComponent,
    DewormingHistoryComponent,
    PatientFormComponent,
    SelectRemoteComponent,
    DewormingFormComponent,
    MedicalHistoryComponent,
    MedicalFormComponent,
    PhysicalHistoryComponent,
    PhysicalFormComponent,
    VaccinationHistoryComponent,
    VaccinationFormComponent,
    PrescriptionHistoryComponent,
    PrescriptionFormComponent,
    ChartGenderComponent
  ],
  exports: [
    MainMenuComponent,
    LoginComponent
  ],
  imports: [
    CommonModule,
    MatButtonModule,
    MatToolbarModule,
    MatMenuModule,
    MatIconModule,
    MatCardModule,
    MatFormFieldModule,
    MatSelectModule,
    MatInputModule,
    FormsModule,
    ReactiveFormsModule,
    MatGridListModule,
    LayoutModule,
    MatTableModule,
    MatProgressBarModule,
    MatPaginatorModule,
    RouterModule,
    MatDividerModule,
    MatTabsModule,
    MatProgressSpinnerModule,
    MatDialogModule,
    MatRadioModule,
    MatDatepickerModule,
    MatCheckboxModule,
    MatAutocompleteModule,
    MatSlideToggleModule,
    NgxEchartsModule,
  ]
})
export class CoreModule {
}
