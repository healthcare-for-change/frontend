import { Component, OnInit, Inject } from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {ConfigurationService} from "../../../configuration.service";
import {Patient} from "../../../_interface/patient.model";
import {DatePipe, NgClass} from "@angular/common";
import {UserService} from "../../../services/user/user.service";
import {MatDialog} from "@angular/material/dialog";
import {PatientFormComponent} from "../patient-form/patient-form.component";

@Component({
  selector: 'app-patient-detail',
  templateUrl: './patient-detail.component.html',
  styleUrls: ['./patient-detail.component.scss']
})
export class PatientDetailComponent implements OnInit {

  uuid: string = '';
  isLoading: boolean = false;
  datePipe = new DatePipe('en-US');
  patient: any = new Object();

  constructor(private route: ActivatedRoute,
              private config: ConfigurationService,
              private userService: UserService,
              private dialog: MatDialog) {
    this.route.params.subscribe( params => {
      this.uuid = params['uuid'];
    })
  }

  ngOnInit(): void {
    this.loadPatientData();
  }

  editDialog(): void {
    const dialogRef = this.dialog.open(PatientFormComponent, {
      width: '450px',
      data: {uuid: this.uuid },
    });

    dialogRef.afterClosed().subscribe(result => {
      // Update patient information in datasheet:
      this.loadPatientData();
    });
  }

  loadPatientData() {
    this.isLoading = true;
    let URL = this.config.getConfiguration('apiEndPoint') + `/patient/details/${this.uuid}`;

    let httpHeaders = this.userService.getHttpHeaders("object");
    fetch(URL, { headers: httpHeaders })
      .then(response => response.json())
      .then( (data:Patient) => {
        this.uuid = data.uuid;
        this.patient = data;
        this.isLoading = false;
      }, error => {
        console.log(error);
        this.isLoading = false;
      })
  }

}
