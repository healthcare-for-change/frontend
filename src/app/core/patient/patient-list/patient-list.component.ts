import {Component, OnInit, ViewChild} from '@angular/core';
import {MatTableDataSource} from "@angular/material/table";
import {Patient} from "../../../_interface/patient.model";
import {MatPaginator, PageEvent} from "@angular/material/paginator";
import {ConfigurationService} from "../../../configuration.service";
import {DatePipe} from "@angular/common";
import {UserService} from "../../../services/user/user.service";
import {PatientFormComponent} from "../patient-form/patient-form.component";
import {MatDialog} from "@angular/material/dialog";
import {Router} from "@angular/router";

@Component({
  selector: 'app-patient-list',
  templateUrl: './patient-list.component.html',
  styleUrls: ['./patient-list.component.scss']
})
export class PatientListComponent implements OnInit {

  isLoading: boolean = false;
  totalRows: number = 0;
  pageSize: number = 10;
  currentPage: number = 0;
  pageSizeOptions: number[] = [10, 25, 50, 100];
  filter: string = "";
  datePipe = new DatePipe('en-US');

  displayedColumns: string[] = ['first_name', 'name', 'date_of_birth'];
  dataSource: MatTableDataSource<Patient> = new MatTableDataSource<Patient>();

  @ViewChild(MatPaginator)
  paginator!: MatPaginator;

  constructor( public config: ConfigurationService,
               private userService: UserService,
               private dialog: MatDialog,
               private router: Router) { }

  ngOnInit(): void {
    this.loadData();
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
  }

  loadData() {
    this.isLoading = true;
    let URL = this.config.getConfiguration('apiEndPoint') + `/patient/list/${this.currentPage}/${this.pageSize}?filter=${this.filter}`;

    let httpHeaders = this.userService.getHttpHeaders("object");
    fetch(URL, { headers: httpHeaders })
      .then(response => response.json())
      .then(data => {
        this.dataSource.data = data.rows;
        setTimeout(() => {
          this.paginator.pageIndex = this.currentPage;
          this.paginator.length = data.count;
        });
        this.isLoading = false;
      }, error => {
        console.log(error);
        this.isLoading = false;
      })
  }

  pageChanged(event: PageEvent) {
    this.pageSize = event.pageSize;
    this.currentPage = event.pageIndex;
    this.loadData();
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.filter = filterValue.trim().toLowerCase();
    this.loadData();
  }

  addDialog(): void {
    const dialogRef = this.dialog.open(PatientFormComponent, {
      width: '450px',
      data: { uuid: '' }
    });

    dialogRef.afterClosed().subscribe(result => {
      // Update patient information in datasheet:
      this.loadData();
    });
  }

  showDetails(row: any) {
    this.router.navigate(['./patient/' + row.uuid], {});
  }
}
