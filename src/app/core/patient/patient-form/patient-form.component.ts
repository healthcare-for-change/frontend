import {Component, Inject, OnInit} from '@angular/core';
import {UntypedFormBuilder, FormControl, Validators} from "@angular/forms";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {UserService} from "../../../services/user/user.service";
import {Patient} from "../../../_interface/patient.model";
import {ConfigurationService} from "../../../configuration.service";
import {PatientService} from "../../../services/patient/patient.service";


@Component({
  selector: 'app-patient-form',
  templateUrl: './patient-form.component.html',
  styleUrls: ['./patient-form.component.scss']
})
export class PatientFormComponent implements OnInit {

  public birthdayHint: string = "YYYY/MM/DD";
  protected uuid: string = '';
  public unclearBirthdayStatus: boolean = false;
  religionApiEndpoint: string = "";
  public httpHeaders = this.userService.getHttpHeaders("object");

  constructor(private formBuilder: UntypedFormBuilder,
              private userService: UserService,
              private config: ConfigurationService,
              private patientService: PatientService,
              private dialogRef: MatDialogRef<PatientFormComponent>,
              @Inject(MAT_DIALOG_DATA) public data: {uuid: string}) {
    this.uuid = data.uuid;
  }

  patientForm = this.formBuilder.group({
    first_name: ['', Validators.required],
    name: ['', Validators.required],
    mobile_number: [''],
    unclear_birthdate: false,
    date_of_birth: [null, Validators.required],
    date_of_death: [null],
    gender: [null],
    religion_id: [null]
  });

  ngOnInit(): void {

    this.religionApiEndpoint = this.config.getConfiguration('apiEndPoint') + `/religion/autocomplete_list/`;

    if(this.uuid) {
      // fetch information from API:
      let URL = this.config.getConfiguration('apiEndPoint') + `/patient/details/${this.uuid}`;

      fetch(URL, { headers: this.httpHeaders })
        .then(response => response.json())
        .then( (data:Patient) => {
          this.patientForm.get("first_name")?.setValue(data.first_name);
          this.patientForm.get("name")?.setValue(data.name);
          this.patientForm.get("mobile_number")?.setValue(data.mobile_number);
          this.patientForm.get("date_of_birth")?.setValue(data.date_of_birth);
          this.patientForm.get("date_of_death")?.setValue(data.date_of_death);
          this.patientForm.get("gender")?.setValue(data.gender);
          this.patientForm.get("religion_id")?.setValue(data.religion_id);
          if(data.unclear_birthdate == true) {
            this.unclearBirthdayStatus = true;
            this.birthdayHint = "YYYY";
          }
        }, error => {
          console.log(error);
        })
    }

  }

  save() {
    if (this.patientForm.valid) {
      let date_of_birth = null;
      let date_of_death = null;

      if(this.patientForm.get("date_of_birth")?.value) {
        date_of_birth = new Date(this.patientForm.get("date_of_birth")?.value);
      }

      if(this.patientForm.get("date_of_death")?.value) {
        date_of_death = new Date(this.patientForm.get("date_of_death")?.value);
      }

      let patient: Patient = {
        date_of_birth: date_of_birth,
        date_of_death: date_of_death,
        first_name: this.patientForm.get("first_name")?.value,
        gender: this.patientForm.get("gender")?.value,
        mobile_number: this.patientForm.get("mobile_number")?.value,
        name: this.patientForm.get("name")?.value,
        religion_id: this.patientForm.get("religion_id")?.value?.autocomplete?.id,
        tenant: this.userService.getUserTenant(),
        unclear_birthdate: this.patientForm.get("unclear_birthdate")?.value,
        uuid: this.uuid
      }

      if(this.uuid !== "") {
        this.patientService.updatePatient(patient).subscribe(status => {
          if(status.status == "success") {
            this.dialogRef.close();
          }
        });
      }
      else {
        this.patientService.addPatient(patient).subscribe(status => {
          if(status.status == "success") {
            this.dialogRef.close();
          }
        });
      }

    }
  }

  unclearBirthdayCheck(event: any)
  {
    if(event.checked) {
      this.birthdayHint = "YYYY";
    }
    else {
      this.birthdayHint = "YYYY/MM/DD";
    }
  }
}
