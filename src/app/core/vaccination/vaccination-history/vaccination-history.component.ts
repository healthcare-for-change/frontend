import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {DatePipe} from "@angular/common";
import {MatTableDataSource} from "@angular/material/table";
import {MatPaginator, PageEvent} from "@angular/material/paginator";
import {ConfigurationService} from "../../../configuration.service";
import {UserService} from "../../../services/user/user.service";
import {MatDialog} from "@angular/material/dialog";
import {Vaccination} from "../../../_interface/vaccination.model";
import {VaccinationFormComponent} from "../vaccination-form/vaccination-form.component";

@Component({
  selector: 'app-vaccination-history',
  templateUrl: './vaccination-history.component.html',
  styleUrls: ['./vaccination-history.component.scss']
})
export class VaccinationHistoryComponent implements OnInit {

  @Input() uuid: string = "";

  isLoading: boolean = false;
  totalRows: number = 0;
  pageSize: number = 10;
  currentPage: number = 0;
  pageSizeOptions: number[] = [10, 25, 50, 100];
  filter: string = "";
  datePipe = new DatePipe('en-US');

  displayedColumns: string[] = ['timestamp', 'vaccine_name', 'comment', 'user_name', 'actions'];
  dataSource: MatTableDataSource<Vaccination> = new MatTableDataSource<Vaccination>();

  @ViewChild(MatPaginator)
  paginator!: MatPaginator;

  constructor(
    public config: ConfigurationService,
    private userService: UserService,
    private dialog: MatDialog) {
  }

  ngOnInit(): void {
    this.loadData();
  }

  loadData() {
    this.isLoading = true;
    let URL = this.config.getConfiguration('apiEndPoint') + `/vaccination/history/${this.uuid}/${this.currentPage}/${this.pageSize}`;
    let httpHeaders = this.userService.getHttpHeaders("object");

    fetch(URL, {headers: httpHeaders})
      .then(response => response.json())
      .then(data => {
        this.dataSource.data = data.rows;
        setTimeout(() => {
          this.paginator.pageIndex = this.currentPage;
          this.paginator.length = data.count;
        });
        this.isLoading = false;
      }, error => {
        console.log(error);
        this.isLoading = false;
      });
  }

  pageChanged(event: PageEvent) {
    this.pageSize = event.pageSize;
    this.currentPage = event.pageIndex;
    this.loadData();
  }

  addDialog(): void {
    this.openDialog('');
  }

  editDialog(uuid: string): void {
    this.openDialog(uuid);
  }

  openDialog(uuid: string|null): void {
    const dialogRef = this.dialog.open(VaccinationFormComponent, {
      width: '450px',
      data: {
        uuid: uuid,
        patientUuid: this.uuid
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      this.loadData();
    });
  }
}
