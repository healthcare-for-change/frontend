import {Component, Inject, OnInit} from '@angular/core';
import {UserService} from "../../../services/user/user.service";
import {UntypedFormBuilder, Validators} from "@angular/forms";
import {ConfigurationService} from "../../../configuration.service";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {VaccinationService} from "../../../services/vaccination/vaccination.service";
import {Vaccination} from "../../../_interface/vaccination.model";

@Component({
  selector: 'app-vaccination-form',
  templateUrl: './vaccination-form.component.html',
  styleUrls: ['./vaccination-form.component.scss']
})
export class VaccinationFormComponent implements OnInit {

  protected uuid: string = '';
  protected patientUuid: string = '';
  public httpHeaders = this.userService.getHttpHeaders("object");
  vaccineApiEndpoint: string = this.config.getConfiguration('apiEndPoint') + `/vaccine/autocomplete_list/`;

  constructor(
    private userService: UserService,
    private formBuilder: UntypedFormBuilder,
    private config: ConfigurationService,
    private vaccinationService: VaccinationService,
    private dialogRef: MatDialogRef<VaccinationFormComponent>,
    @Inject(MAT_DIALOG_DATA) public data: {
      uuid: string,
      patientUuid: string
    }) {
    this.uuid = data.uuid;
    this.patientUuid = data.patientUuid;
  }

  form = this.formBuilder.group({
    timestamp: [new Date(), Validators.required],
    vaccine_uuid: ['', Validators.required],
    comment: ['', null],
    lot: ['', null],
  });

  ngOnInit(): void {
    if (this.uuid) {
      let URL = this.config.getConfiguration('apiEndPoint') + `/vaccination/details/${this.uuid}`;

      fetch(URL, {headers: this.httpHeaders})
        .then(response => response.json())
        .then((data: Vaccination) => {
          this.form.get("patient_uuid")?.setValue(data.patient_uuid);
          this.form.get("timestamp")?.setValue(data.timestamp);
          this.form.get("vaccine_uuid")?.setValue(data.vaccine_uuid);
          this.form.get("lot")?.setValue(data.lot);
          this.form.get("comment")?.setValue(data.comment);
        }, error => {
          console.log(error);
        })
    }
    else {
      this.form.get("timestamp")?.setValue(new Date());
    }
  }

  save() {
    if (this.form.valid) {
      let timestamp = new Date();
      if (this.form.get("timestamp")?.value) {
        timestamp = new Date(this.form.get("timestamp")?.value);
      }

      let vaccination: Vaccination = {
        lot: this.form.get("lot")?.value,
        comment: this.form.get("comment")?.value,
        vaccine_uuid: this.form.get("vaccine_uuid")?.value?.autocomplete?.id,
        patient_uuid: this.patientUuid,
        timestamp: timestamp,
        user_id: 0, // Use for model compliance
        uuid: this.uuid
      }

      if (this.uuid !== "") {
        this.vaccinationService.updateVaccination(vaccination).subscribe(status => {
          if (status.status == "success") {
            this.dialogRef.close();
          }
        });
      } else {
        this.vaccinationService.addVaccination(vaccination).subscribe(status => {
          if (status.status == "success") {
            this.dialogRef.close();
          }
        });
      }
    }
  }
}
