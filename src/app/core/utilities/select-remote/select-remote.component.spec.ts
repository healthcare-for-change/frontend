import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectRemoteComponent } from './select-remote.component';

describe('SelectRemoteComponent', () => {
  let component: SelectRemoteComponent;
  let fixture: ComponentFixture<SelectRemoteComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SelectRemoteComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectRemoteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
