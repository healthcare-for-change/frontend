import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {
  ControlValueAccessor,
  UntypedFormBuilder,
  UntypedFormControl,
  NG_VALUE_ACCESSOR,
} from "@angular/forms";
import {HttpClient, HttpParams} from "@angular/common/http";
import {debounceTime, distinctUntilChanged, finalize, Observable, startWith, Subscription, switchMap, tap} from "rxjs";
import {UserService} from "../../../services/user/user.service";

@Component({
  selector: 'utilities-select-remote',
  templateUrl: './select-remote.component.html',
  styleUrls: ['./select-remote.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      multi: true,
      useExisting: SelectRemoteComponent
    }
  ]
})
export class SelectRemoteComponent implements OnInit, ControlValueAccessor, OnDestroy {

  public isLoading: boolean = false;
  public selectedItem: any;
  @Input() source: string = "";
  @Input() itemId: string = "";
  @Input() itemClass: string = "";
  @Input() label: string = "";
  @Input() formControlName: string = "";
  @Input() requestHeaders: string = "";
  @Input() value: string = "";

  onTouched: Function = () => {};
  onChangeSubs: Subscription[] = [];

  selectRemoteControl = new UntypedFormControl();
  filteredItems: any;
  errorMsg: string = "";

  formGroup = this.formBuilder.group({
    autocomplete: []
  })

  constructor(
    private formBuilder: UntypedFormBuilder,
    private http: HttpClient,
    private userService: UserService) {
  }

  writeValue(obj: any): void {
    if (obj) {
      this.formGroup.setValue({
        autocomplete: {id: obj, name: "Keep current" }
      }, {emitEvent: true});
    }
  }

  registerOnChange(onChange: any): void {
    const sub = this.formGroup.valueChanges.subscribe(onChange);
    this.onChangeSubs.push(sub);
  }

  registerOnTouched(onTouched: Function) {
    this.onTouched = onTouched;
  }

  ngOnDestroy() {
    for (let sub of this.onChangeSubs) {
      sub.unsubscribe();
    }
  }

  setDisabledState(disabled: boolean) {
    if (disabled) {
      this.formGroup.disable();
    }
    else {
      this.formGroup.enable();
    }
  }

  ngOnInit(): void {
    let httpHeaders = this.userService.getHttpHeaders("object");
    const initialQueryFieldData = "";

    this.formGroup.get("autocomplete")?.valueChanges
      .pipe(
        startWith(initialQueryFieldData),
        distinctUntilChanged(),
        debounceTime(1000),
        tap(() => {
          this.errorMsg = "";
          this.filteredItems = [];
          this.isLoading = true;
        }),
        switchMap(value => (this.http.get(this.source, {
          headers: httpHeaders,
          params: (new HttpParams().set("q", value))
        }))
          .pipe(
            finalize(() => {
              this.isLoading = false;
            })
          )
        )
      )
      .subscribe((data: any) => {
        this.filteredItems = data;
      })
  }

  clearSelection() {
    this.selectedItem = null;
    this.filteredItems = [];
  }

  onSelected() {
    console.log(this.selectedItem);
  }

  displayFn(value: any) {
    return value && value.name ? value.name : '';
  }

  onKeyUp(value: any) : void {

  }

  filter(value: string): Observable<any> {
    let httpHeaders = this.userService.getHttpHeaders("object");

    return (this.http.get(this.source, {
      headers: httpHeaders,
      params: (new HttpParams().set("q", value))
    }))
      .pipe(
        finalize(() => {
          this.isLoading = false;
        })
      )
  }
}
