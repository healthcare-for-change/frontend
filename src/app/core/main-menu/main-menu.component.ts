import {Component, OnInit} from '@angular/core';
import {UserService} from "../../services/user/user.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-main-menu',
  templateUrl: './main-menu.component.html',
  styleUrls: ['./main-menu.component.scss']
})
export class MainMenuComponent implements OnInit {

  isLoggedIn: boolean = this.userService.getLoginStatus();
  userName: string = this.userService.getUserName();

  constructor(private userService: UserService, private router: Router) {
    this.userService.loginStatusChange.subscribe(value => {
      this.isLoggedIn = value;
    });

    this.userService.userNameChange.subscribe(value => {
      this.userName = value;
    });
  }

  ngOnInit(): void {
  }

  logout() {
    this.userService.logout();
  }
}
