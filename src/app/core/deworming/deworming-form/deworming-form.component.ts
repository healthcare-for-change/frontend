import {Component, Inject, OnInit} from '@angular/core';
import {UserService} from "../../../services/user/user.service";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {UntypedFormBuilder, Validators} from "@angular/forms";
import {ConfigurationService} from "../../../configuration.service";
import {Deworming} from "../../../_interface/deworming.model";
import {DewormingService} from "../../../services/deworming/deworming.service";

@Component({
  selector: 'app-deworming-form',
  templateUrl: './deworming-form.component.html',
  styleUrls: ['./deworming-form.component.scss']
})
export class DewormingFormComponent implements OnInit {

  protected uuid: string = '';
  protected patientUuid: string = '';
  public httpHeaders = this.userService.getHttpHeaders("object");
  medicationApiEndpoint: string = this.config.getConfiguration('apiEndPoint') + `/medication/autocomplete_list/`;

  constructor(private userService: UserService,
              private formBuilder: UntypedFormBuilder,
              private config: ConfigurationService,
              private dewormingService: DewormingService,
              private dialogRef: MatDialogRef<DewormingFormComponent>,
              @Inject(MAT_DIALOG_DATA) public data: {
                uuid: string,
                patientUuid: string
              }) {
    this.uuid = data.uuid;
    this.patientUuid = data.patientUuid;
  }

  form = this.formBuilder.group({
    timestamp: ['', Validators.required],
    medication_id: ['', null],
    comment: ['', null],
  });

  ngOnInit(): void {
    if (this.uuid) {
      let URL = this.config.getConfiguration('apiEndPoint') + `/deworming/details/${this.uuid}`;

      fetch(URL, {headers: this.httpHeaders})
        .then(response => response.json())
        .then((data: Deworming) => {
          this.form.get("patient_uuid")?.setValue(data.patient_uuid);
          this.form.get("timestamp")?.setValue(data.timestamp);
          this.form.get("medication_id")?.setValue(data.medication_id);
          this.form.get("comment")?.setValue(data.comment);
        }, error => {
          console.log(error);
        })
    }
    else {
      this.form.get("timestamp")?.setValue(new Date());
    }
  }

  save() {
    if (this.form.valid) {
      let timestamp = null;
      if (this.form.get("timestamp")?.value) {
        timestamp = new Date(this.form.get("timestamp")?.value);
      }

      let deworming: Deworming = {
        comment: this.form.get("comment")?.value,
        medication_id: this.form.get("medication_id")?.value?.autocomplete?.id,
        patient_uuid: this.patientUuid,
        timestamp: timestamp,
        user_id: 0, // Use for model compliance
        uuid: this.uuid
      }

      if (this.uuid !== "") {
        this.dewormingService.updateDeworming(deworming).subscribe(status => {
          if (status.status == "success") {
            this.dialogRef.close();
          }
        });
      } else {
        this.dewormingService.addDeworming(deworming).subscribe(status => {
          if (status.status == "success") {
            this.dialogRef.close();
          }
        });
      }

    }
  }

}
