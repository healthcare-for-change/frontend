import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DewormingHistoryComponent } from './deworming-history.component';

describe('DewormingHistoryComponent', () => {
  let component: DewormingHistoryComponent;
  let fixture: ComponentFixture<DewormingHistoryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DewormingHistoryComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DewormingHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
