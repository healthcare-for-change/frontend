import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {DatePipe} from "@angular/common";
import {MatTableDataSource} from "@angular/material/table";
import {Patient} from "../../../_interface/patient.model";
import {MatPaginator, PageEvent} from "@angular/material/paginator";
import {ConfigurationService} from "../../../configuration.service";
import {UserService} from "../../../services/user/user.service";
import {PatientFormComponent} from "../../patient/patient-form/patient-form.component";
import {MatDialog} from "@angular/material/dialog";
import {DewormingFormComponent} from "../deworming-form/deworming-form.component";

@Component({
  selector: 'app-deworming-history',
  templateUrl: './deworming-history.component.html',
  styleUrls: ['./deworming-history.component.scss']
})
export class DewormingHistoryComponent implements OnInit {

  @Input() uuid: string = "";

  isLoading: boolean = false;
  totalRows: number = 0;
  pageSize: number = 10;
  currentPage: number = 0;
  pageSizeOptions: number[] = [10, 25, 50, 100];
  filter: string = "";
  datePipe = new DatePipe('en-US');

  displayedColumns: string[] = ['timestamp', 'medication_name', 'user_name'];
  dataSource: MatTableDataSource<Patient> = new MatTableDataSource<Patient>();

  @ViewChild(MatPaginator)
  paginator!: MatPaginator;

  constructor(
    public config: ConfigurationService,
    private userService: UserService,
    private dialog: MatDialog) {
  }

  ngOnInit(): void {
    this.loadData();
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
  }

  loadData() {
    this.isLoading = true;
    let URL = this.config.getConfiguration('apiEndPoint') + `/deworming/history/${this.uuid}/${this.currentPage}/${this.pageSize}`;

    let httpHeaders = this.userService.getHttpHeaders("object");
    fetch(URL, {headers: httpHeaders})
      .then(response => response.json())
      .then(data => {
        this.dataSource.data = data.rows;
        setTimeout(() => {
          this.paginator.pageIndex = this.currentPage;
          this.paginator.length = data.count;
        });
        this.isLoading = false;
      }, error => {
        console.log(error);
        this.isLoading = false;
      })
  }

  pageChanged(event: PageEvent) {
    this.pageSize = event.pageSize;
    this.currentPage = event.pageIndex;
    this.loadData();
  }

  addDialog(): void {
    const dialogRef = this.dialog.open(DewormingFormComponent, {
      width: '450px',
      data: {
        uuid: '',
        patientUuid: this.uuid
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      // Update patient information in datasheet:
      this.loadData();
    });
  }

}
