import {Component, Inject, OnInit} from '@angular/core';
import {UserService} from "../../../services/user/user.service";
import {UntypedFormBuilder, Validators} from "@angular/forms";
import {ConfigurationService} from "../../../configuration.service";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {PhysicalExamination} from "../../../_interface/pysical_examination.model";
import {PhysicalGeneralImpression} from "../../../_interface/physical-general-impression.model";
import {MatDivider} from "@angular/material/divider";
import {PhysicalService} from "../../../services/physical/physical.service";

@Component({
  selector: 'app-physical-form',
  templateUrl: './physical-form.component.html',
  styleUrls: ['./physical-form.component.scss']
})
export class PhysicalFormComponent implements OnInit {

  protected uuid: string = '';
  protected patientUuid: string = '';
  public httpHeaders = this.userService.getHttpHeaders("object");

  public generalImpression: Array<PhysicalGeneralImpression> = [
    {id: 1, name: "healthy impression"},
    {id: 2, name: "minor medical impairments"},
    {id: 3, name: "major medical impairments"},
    {id: 4, name: "critical medical impairments"},
  ];

  form = this.formBuilder.group({
    timestamp: ['', Validators.required],
    blood_pressure_systolic: ['', null],
    blood_pressure_diastolic: ['', null],
    heart_rate: ['', null],
    weight: ['', null],
    height: ['', null],
    muac: ['', null],
    temperature: ['', null],
    skin_examination: ['', null],
    details_ent: ['', null],
    details_eyes: ['', null],
    details_head: ['', null],
    details_muscles_bones: ['', null],
    details_heart: ['', null],
    details_lung: ['', null],
    details_gastrointestinal: ['', null],
    details_urinary_tract: ['', null],
    details_reproductive_system: ['', null],
    details_other: ['', null],
    general_impression: ['', null]
  });

  constructor(
    private userService: UserService,
    private formBuilder: UntypedFormBuilder,
    private config: ConfigurationService,
    private dialogRef: MatDialogRef<PhysicalFormComponent>,
    private physicalService: PhysicalService,
    @Inject(MAT_DIALOG_DATA) public data: {
      uuid: string,
      patientUuid: string
    }
  ) {
    this.uuid = data.uuid;
    this.patientUuid = data.patientUuid;
  }

  ngOnInit(): void {
    if (this.uuid) {
      let URL = this.config.getConfiguration('apiEndPoint') + `/physical/details/${this.uuid}`;

      fetch(URL, {headers: this.httpHeaders})
        .then(response => response.json())
        .then((data: PhysicalExamination) => {
          this.form.get("patient_uuid")?.setValue(data.patient_uuid);
          this.form.get("timestamp")?.setValue(data.timestamp);
          this.form.get("blood_pressure_systolic")?.setValue(data.blood_pressure_systolic);
          this.form.get("blood_pressure_diastolic")?.setValue(data.blood_pressure_diastolic);
          this.form.get("heart_rate")?.setValue(data.heart_rate);
          this.form.get("weight")?.setValue(data.weight);
          this.form.get("height")?.setValue(data.height);
          this.form.get("muac")?.setValue(data.muac);
          this.form.get("temperature")?.setValue(data.temperature);
          this.form.get("skin_examination")?.setValue(data.skin_examination);
          this.form.get("details_ent")?.setValue(data.details_ent);
          this.form.get("details_eyes")?.setValue(data.details_eyes);
          this.form.get("details_head")?.setValue(data.details_head);
          this.form.get("details_muscles_bones")?.setValue(data.details_muscles_bones);
          this.form.get("details_heart")?.setValue(data.details_heart);
          this.form.get("details_lung")?.setValue(data.details_lung);
          this.form.get("details_gastrointestinal")?.setValue(data.details_gastrointestinal);
          this.form.get("details_urinary_tract")?.setValue(data.details_urinary_tract);
          this.form.get("details_reproductive_system")?.setValue(data.details_reproductive_system);
          this.form.get("details_other")?.setValue(data.details_other);
          this.form.get("general_impression")?.setValue(data.general_impression);
        }, error => {
          console.log(error);
        })
    }
  }

  save() {
    if (this.form.valid) {
      let physical: PhysicalExamination = {
        user_id: 0, // Use for model compliance, will be handled by REST controller
        patient_uuid: this.patientUuid,
        uuid: this.uuid,
        blood_pressure_diastolic: this.form.get("blood_pressure_diastolic")?.value,
        blood_pressure_systolic: this.form.get("blood_pressure_systolic")?.value,
        details_ent: this.form.get("details_ent")?.value,
        details_eyes: this.form.get("details_eyes")?.value,
        details_gastrointestinal: this.form.get("details_gastrointestinal")?.value,
        details_head: this.form.get("details_head")?.value,
        details_heart: this.form.get("details_heart")?.value,
        details_lung: this.form.get("details_lung")?.value,
        details_muscles_bones: this.form.get("details_muscles_bones")?.value,
        details_other: this.form.get("details_other")?.value,
        details_reproductive_system: this.form.get("details_reproductive_system")?.value,
        details_urinary_tract: this.form.get("details_urinary_tract")?.value,
        general_impression: this.form.get("general_impression")?.value,
        heart_rate: this.form.get("heart_rate")?.value,
        height: this.form.get("height")?.value,
        muac: this.form.get("muac")?.value,
        skin_examination: this.form.get("skin_examination")?.value,
        temperature: this.form.get("temperature")?.value,
        timestamp: this.form.get("timestamp")?.value,
        weight: this.form.get("weight")?.value
      };

      if (this.uuid !== "") {
        this.physicalService.updatePhysicalExamination(physical).subscribe(status => {
          if (status.status == "success") {
            this.dialogRef.close();
          }
        });
      } else {
        this.physicalService.addPhysicalExamination(physical).subscribe(status => {
          if (status.status == "success") {
            this.dialogRef.close();
          }
        });
      }
    }
  }
}
