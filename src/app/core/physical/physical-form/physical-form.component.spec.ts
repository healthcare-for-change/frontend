import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PhysicalFormComponent } from './physical-form.component';

describe('PhysicalFormComponent', () => {
  let component: PhysicalFormComponent;
  let fixture: ComponentFixture<PhysicalFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PhysicalFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PhysicalFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
