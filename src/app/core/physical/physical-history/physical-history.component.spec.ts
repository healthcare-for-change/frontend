import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PhysicalHistoryComponent } from './physical-history.component';

describe('PhysicalHistoryComponent', () => {
  let component: PhysicalHistoryComponent;
  let fixture: ComponentFixture<PhysicalHistoryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PhysicalHistoryComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PhysicalHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
