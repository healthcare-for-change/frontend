import {Component, Inject, OnInit} from '@angular/core';
import {UserService} from "../../../services/user/user.service";
import {UntypedFormBuilder, Validators} from "@angular/forms";
import {ConfigurationService} from "../../../configuration.service";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {MedicalService} from "../../../services/medical/medical.service";
import {MedicalHistory} from "../../../_interface/medical_history.model";
import {MedicationClinicalStatus} from "../../../_interface/medication-clinical-status.model";
import {MedicationSeverity} from "../../../_interface/medication-severity.model";
import {MedicationVerificationStatus} from "../../../_interface/medication-verification-status.model";

@Component({
  selector: 'app-medical-form',
  templateUrl: './medical-form.component.html',
  styleUrls: ['./medical-form.component.scss']
})
export class MedicalFormComponent implements OnInit {

  protected uuid: string = '';
  protected patientUuid: string = '';
  public httpHeaders = this.userService.getHttpHeaders("object");
  medicalDiagnosisApiEndpoint: string = this.config.getConfiguration('apiEndPoint') + `/medical_diagnosis/autocomplete_list/`;

  public clinicalStatus: Array<MedicationClinicalStatus> = [
    {id: 1, name: "active"},
    {id: 2, name: "inactive"},
    {id: 3, name: "resolved"},
  ];

  public verificationStatus: Array<MedicationVerificationStatus> = [
    {id: 1, name: "unconfirmed"},
    {id: 2, name: "provisional"},
    {id: 3, name: "refuted"},
  ];

  public severity: Array<MedicationSeverity> = [
    {id: 1, name: "severe"},
    {id: 2, name: "moderate"},
    {id: 3, name: "mild"},
  ];

  form = this.formBuilder.group({
    medical_diagnosis_code_id: ['', Validators.required],
    clinical_status: ['', Validators.required],
    verification_status: ['', Validators.required],
    severity: ['', Validators.required],
    note: ['', null],
    onset_date: ['', null],
    onset_age: ['', null],
    abatement_date: ['', null],
    abatement_age: ['', null]
  });

  constructor(private userService: UserService,
              private formBuilder: UntypedFormBuilder,
              private config: ConfigurationService,
              private medicalService: MedicalService,
              private dialogRef: MatDialogRef<MedicalFormComponent>,
              @Inject(MAT_DIALOG_DATA) public data: {
                uuid: string,
                patientUuid: string
              }) {
    this.uuid = data.uuid;
    this.patientUuid = data.patientUuid;
  }

  ngOnInit(): void {
    if (this.uuid) {
      let URL = this.config.getConfiguration('apiEndPoint') + `/medical/details/${this.uuid}`;

      fetch(URL, {headers: this.httpHeaders})
        .then(response => response.json())
        .then((data: MedicalHistory) => {
          this.form.get("patient_uuid")?.setValue(data.patient_uuid);
          this.form.get("clinical_status")?.setValue(data.clinical_status);
          this.form.get("verification_status")?.setValue(data.verification_status);
          this.form.get("severity")?.setValue(data.severity);
          this.form.get("onset_age")?.setValue(data.onset_age);
          this.form.get("onset_date")?.setValue(data.onset_date);
          this.form.get("abatement_age")?.setValue(data.abatement_age);
          this.form.get("abatement_date")?.setValue(data.abatement_date);
          this.form.get("note")?.setValue(data.note);
          this.form.get("medical_diagnosis_code_id")?.setValue(data.medical_diagnosis_code_id);
        }, error => {
          console.log(error);
        })
    }
  }

  save() {
    if (this.form.valid) {
      let timestamp = null;
      if (this.form.get("timestamp")?.value) {
        timestamp = new Date(this.form.get("timestamp")?.value);
      }

      let medical: MedicalHistory = {
        patient_uuid: this.patientUuid,
        user_id: 0, // Use for model compliance, will be handled by REST controller
        clinical_status: this.form.get("clinical_status")?.value,
        verification_status: this.form.get("verification_status")?.value,
        severity: this.form.get("severity")?.value,
        onset_age: this.form.get("onset_age")?.value,
        onset_date: this.form.get("onset_date")?.value,
        abatement_age: this.form.get("abatement_age")?.value,
        abatement_date: this.form.get("abatement_date")?.value,
        note: this.form.get("note")?.value,
        medical_diagnosis_code_id: this.form.get("medical_diagnosis_code_id")?.value?.autocomplete?.id,
        uuid: this.uuid
      }

      if (this.uuid !== "") {
        this.medicalService.updateMedicalHistory(medical).subscribe(status => {
          if (status.status == "success") {
            this.dialogRef.close();
          }
        });
      } else {
        this.medicalService.addMedicalHistory(medical).subscribe(status => {
          if (status.status == "success") {
            this.dialogRef.close();
          }
        });
      }

    }
  }
}
