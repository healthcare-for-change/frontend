import {Component, Inject, OnInit} from '@angular/core';
import {UserService} from "../../../services/user/user.service";
import {UntypedFormBuilder, Validators} from "@angular/forms";
import {ConfigurationService} from "../../../configuration.service";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {PrescriptionService} from "../../../services/prescription/prescription.service";
import {Vaccination} from "../../../_interface/vaccination.model";
import {Prescription} from "../../../_interface/Prescription.model";

@Component({
  selector: 'app-prescription-form',
  templateUrl: './prescription-form.component.html',
  styleUrls: ['./prescription-form.component.scss']
})
export class PrescriptionFormComponent implements OnInit {

  protected uuid: string = '';
  protected patientUuid: string = '';
  public httpHeaders = this.userService.getHttpHeaders("object");
  medicationApiEndpoint: string = this.config.getConfiguration('apiEndPoint') + `/medication/autocomplete_list/`;

  constructor(
    private userService: UserService,
    private formBuilder: UntypedFormBuilder,
    private config: ConfigurationService,
    private prescriptionService: PrescriptionService,
    private dialogRef: MatDialogRef<PrescriptionFormComponent>,
    @Inject(MAT_DIALOG_DATA) public data: {
      uuid: string,
      patientUuid: string
    }) {
    this.uuid = data.uuid;
    this.patientUuid = data.patientUuid;
  }

  form = this.formBuilder.group({
    medication_uuid: ['', Validators.required],
    dose_quantity: ['', null],
    dose_morning: ['', null],
    dose_noon: ['', null],
    dose_evening: ['', null],
    dose_night: ['', null],
    max_dose_period: ['', null],
    patient_instruction: ['', null],
    dose_start: ['', null],
    dose_end: ['', null],
    interval: ['', null],
    comment: ['', null],
  });

  ngOnInit(): void {
    if (this.uuid) {
      let URL = this.config.getConfiguration('apiEndPoint') + `/prescription/details/${this.uuid}`;

      fetch(URL, {headers: this.httpHeaders})
        .then(response => response.json())
        .then((data: Prescription) => {
          this.form.get("medication_uuid")?.setValue(data.medication_uuid);
          this.form.get("dose_quantity")?.setValue(data.dose_quantity);
          this.form.get("dose_morning")?.setValue(data.dose_morning);
          this.form.get("dose_noon")?.setValue(data.dose_noon);
          this.form.get("dose_evening")?.setValue(data.dose_evening);
          this.form.get("dose_night")?.setValue(data.dose_night);
          this.form.get("max_dose_period")?.setValue(data.max_dose_period);
          this.form.get("patient_instruction")?.setValue(data.patient_instruction);
          this.form.get("dose_start")?.setValue(data.dose_start);
          this.form.get("dose_end")?.setValue(data.dose_end);
          this.form.get("interval")?.setValue(data.interval);
          this.form.get("comment")?.setValue(data.comment);
          this.form.get("patient_uuid")?.setValue(data.patient_uuid);
        }, error => {
          console.log(error);
        })
    }
  }

  save() {
    if (this.form.valid) {
      let start = null;
      let end = null;

      if(this.form.get("dose_start")?.value) {
        start = new Date(this.form.get("dose_start")?.value);
      }

      if(this.form.get("dose_end")?.value) {
        end = new Date(this.form.get("dose_end")?.value);
      }

      let prescription: Prescription = {
        uuid: this.uuid,
        patient_uuid: this.patientUuid,
        user_id: 0, // Use for model compliance
        created_at: null,
        dose_evening: this.form.get("dose_evening")?.value,
        dose_morning: this.form.get("dose_morning")?.value,
        dose_night: this.form.get("dose_night")?.value,
        dose_noon: this.form.get("dose_noon")?.value,
        dose_quantity: this.form.get("dose_quantity")?.value,
        dose_end: end,
        interval: this.form.get("interval")?.value,
        max_dose_period: this.form.get("max_dose_period")?.value,
        patient_instruction: this.form.get("patient_instruction")?.value,
        dose_start: start,
        updated_at: this.form.get("updated_at")?.value,
        medication_uuid: this.form.get("medication_uuid")?.value?.autocomplete?.id,
        comment: this.form.get("comment")?.value,
      }

      if (this.uuid !== "") {
        this.prescriptionService.updatePrescription(prescription).subscribe(status => {
          if (status.status == "success") {
            this.dialogRef.close();
          }
        });
      } else {
        this.prescriptionService.addPrescription(prescription).subscribe(status => {
          if (status.status == "success") {
            this.dialogRef.close();
          }
        });
      }
    }
  }
}
